/**
 * CalendarController
 *
 * @description :: Server-side logic for managing calendars
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var async  = require('async'),
	lodash = require('lodash'),
	moment = require('moment'),
	romcal = require('romcal');

module.exports = {

	/* RESTful APIs */

	calendar: function( req, res ) {

		var year = req.param('year') || moment.utc().year();
		if ( lodash.isString( year ) ) 
			year = parseInt( year );

		var actions = {
			calendars: function( cb ) {
				romcal.list('calendars', function( err, calendars ) {
					if( err ) cb( err );
					else cb( null, JSON.parse( calendars ) );
				});
			},
			months: function( cb ) {
				romcal.list('months', function( err, months ) {
					if ( err ) cb( err );
					else cb( null, JSON.parse( months ));
				});
			},
			days: function( cb ) {
				romcal.list('days', function( err, days ) {
					if ( err ) cb( err );
					else cb( null, JSON.parse( days ));
				});
			},
			dates: function( cb ) {
				romcal.calendarFor( year, function( err, dates ) {
					if( err ) cb( err );
					else {
						romcal.queryFor('weeksByMonthGrouped', dates, function( erz, query ) {
							if ( erz ) cb ( erz );
							else cb( null, JSON.parse( query ) );
						});
					}
				});
			}
		};
		
		async.parallel( actions, function( error, output ) {

			if ( error ) {
				sails.log.error( error );
				return res.json({
					success: false,
					data: {
						error: error
					}
				});
			}

			var constants = {
					calendars: output.calendars,
					months: output.months,
					days: output.days
				},
				weeksByMonth = output.dates;

			lodash.map( weeksByMonth, function( item, k ) {
				console.log( item.month.name );
				lodash.map( item.weeks, function( week, ke ) {
					console.log( ke );
					lodash.map( week, function( date, key ) {
						if ( !lodash.isEmpty( date ) ) {
							date.moment = moment.utc( date.timestamp );
							date.date = date.moment.date();
						}
					});
				});
			});

			// Assign first week of next year as 'last' week of this year for display purposes
			var lastWeek = weeksByMonth[ 11 ].weeks[ 1 ];
			weeksByMonth[ 11 ].weeks[ lastWeek[0].moment.weeksInYear() + 1 ] = lastWeek;
			delete weeksByMonth[11].weeks[1];

			lodash.sortBy( weeksByMonth, function( item ) {
				return item.month.order;
			});


			// Only serve ajax requests
			if ( req.wantsJSON ) {
				res.json({ 
					success: true,
					data: {
						constants: constants, 
						dates: weeksByMonth 
					}
				});
			}
		});
	}
};

