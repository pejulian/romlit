'use strict';

romlit.controller('CalendarCtrl', [
	'$log',
  '$routeParams',
	'$scope',
	'lodash',
	'calendar.service',

	function( $log, $routeParams, $scope, lodash, Calendar ) {

		// Get the year to fetch
		var year = $routeParams.year || moment.utc().year();
		if ( lodash.isString( year ) )
			year = parseInt( year );

		var now = moment.utc();
		$scope.currentMonth = now.month();
		
		Calendar.romcal().calendarFor( year, 
			function( response ) {
				if ( response.success ) {
					$scope.constants = response.data.constants;
					var dates = response.data.dates;
					$scope.dates = dates;
				}
				else {
					$log.error( response.data.error );
				}
			},
			function( error ) {
				console.log('error', error );
			}
		);
	}
]);