'use strict';

romlit.factory('calendar.service', [
  '$resource', 
  function( $resource ) {
    return {
      romcal: function() {
        return $resource( '/calendar', 
          { year: '@year' }, {
          calendarFor: {
            method: 'GET',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            isArray: false
          }
        });
      }
    }
  }
]); 