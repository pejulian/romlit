'use strict';

angular
  .module('ui.router', [])
  .constant( 'ui.router', {
    path: function( $provider ) {
      $provider
        .when('/calendar', {
            templateUrl: 'js/app/templates/calendar.html',
            controller: 'CalendarCtrl',
            active: 'calendar'
        })
        .when('/calendar/:year', {
            templateUrl: 'js/app/templates/calendar.html',
            controller: 'CalendarCtrl',
            active: 'calendar'            
        })
        .otherwise({
            redirectTo: '/calendar'
        });
    }
  }
);
