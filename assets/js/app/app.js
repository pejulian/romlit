'use strict';

var romlit = angular.module('romlit', [
    'lodash',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ui.router',
    'ui.bootstrap'
]);

romlit.config([
    '$routeProvider',
    '$httpProvider',
    'ui.router',

    function ($routeProvider, $httpProvider, $router) {
        // delete $httpProvider.defaults.headers.common['X-Requested-With'];
        return $router.path( $routeProvider );
    }
]);

romlit.run([

    '$rootScope',
    '$log',

    function( $rootScope, $log ) {
    	
        $rootScope.$on("$routeChangeSuccess", function( opts, route ) { 
            $log.debug( '$routeChangeSuccess', route['loadedTemplateUrl'] );
        });
    }

]);